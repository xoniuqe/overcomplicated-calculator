use std::fmt;
use crate::lib::lexer::{Token, Value, Operator};


#[derive(Debug)]
#[derive(Clone, PartialEq)]
pub enum Tree {
    Value(Value),
    Tree {op: Operator, operands: Vec<Tree>},
    Parenthesis(Box<Tree>),
    Negation(Box<Tree>)
}

impl fmt::Display for Tree {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match self {
            Tree::Value(value) => write!(f, "{:?}", value),
            Tree::Negation(tree) => write!(f, "-{}", tree),
            Tree::Parenthesis(tree) => write!(f,"[{}] ", tree),
            Tree::Tree {op, operands } => {
                write!(f, "({:?} (", op)?;
                for operand in operands {
                    write!(f, "{} ", operand)?;
                }
                write!(f,"))")
            }
        }
    }
}

#[derive(Debug)]
pub enum ParseError {
    Generic(String),
    Unexpected((String, i32)),
}

impl fmt::Display for ParseError {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match self {
            ParseError::Generic(msg) => write!(f, "error {}", msg),
            ParseError::Unexpected((msg, index)) => {
                for _ in 0..*index {
                    write!(f, " ")?;
                }
                writeln!(f, "^")?;
                writeln!(f, "error {}", msg)
            }
        }
    }
}

fn combine(tree: Tree, next_op: Operator, next: Tree) -> Tree {
    if let Tree::Tree { op, operands: _operands } = tree {
        if op == next_op {
            let mut operands = _operands;
            operands.push(next);
            Tree::Tree { op, operands }
        } else if op > next_op {
            Tree::Tree { op: next_op, operands: vec![Tree::Tree { op, operands: _operands }, next]}
        }
        else {
            let mut operands = _operands.clone();
            match operands.pop().unwrap() {
                last @ Tree::Value(_) |
                last @ Tree::Negation(_) |
                last @ Tree::Parenthesis(_) => {
                    operands.push(Tree::Tree{op: next_op, operands: vec![last, next]});
                    Tree::Tree{op, operands }
                },
                last  => {
                    let tmp = combine(last, next_op, next);
                    operands.push(tmp);
                    Tree::Tree{op, operands }
                }
            }
        }
    }
    else {
        Tree::Tree{op: next_op, operands: vec![tree, next]}
    }
}

pub fn parse(tokens: &mut Vec<(Token, i32)>) -> Result<Tree,ParseError> {
    fn parse_next(tokens: &mut Vec<(Token, i32)>) -> Result<Tree, ParseError> {
        match tokens.pop() {
            Some((Token::LeftP, _)) => {
                let tree = inner_parse(tokens, true)?;
                match tokens.last() {
                    Some((Token::RightP, _)) => tokens.pop(),
                    Some((t, p)) => return Err(ParseError::Unexpected((format!("Unexpected {} at {}, expected {}", t, p, Token::RightP), *p))),
                    _ => {
                        println!("test: {:?}", tree);
                        return Err(ParseError::Generic(format!("Missing final right parenthesis!")))
                    }
                };
                Ok(Tree::Parenthesis(Box::from(tree)))
            }
            Some((Token::Operator(Operator::Sub), _)) => Ok(Tree::Negation(Box::from(parse_next(tokens)?))),
            Some((Token::Literal(value), _)) => Ok(Tree::Value(value)),
            Some((t, pos)) => Err(ParseError::Unexpected((format!("Unexpected {} at {}", t, pos),pos))),
            _ => Err(ParseError::Generic(format!("Unexpected ending")))
        }
    }
    fn parse_operator(tokens: &mut Vec<(Token, i32)>) -> Result<Operator, ParseError> {
        match tokens.pop() {
            Some((Token::Operator(operator), _)) => Ok(operator),
            Some((t, pos)) => Err(ParseError::Unexpected((format!("{} is no operator, at {}", t, pos), pos))),
            _ => Err(ParseError::Generic("Unexpected ending".to_string()))
        }
    }

    fn inner_parse(tokens: &mut Vec<(Token, i32)>, check_for_brackets: bool) -> Result<Tree, ParseError> {
        let parsed = parse_next(tokens);
        if tokens.is_empty() {
            parsed
        } else {
            let mut result = parsed?;
            while !tokens.is_empty() {
                if check_for_brackets {
                    if let (Token::RightP, _) = tokens.last().unwrap() {
                        break;
                    }
                }
                result = combine(result, parse_operator(tokens)?, parse_next(tokens)?);
            }
            Ok(result)
        }
    }
    tokens.reverse();
    inner_parse(tokens, false)
}

#[cfg(test)]
mod tests {
    use super::{Tree, combine};
    use crate::lib::lexer::{Value, Operator};

    #[test]
    fn test_combine_values() {
        let left = Tree::Value(Value::Number(1.0));
        let op = Operator::Mul;
        let right = Tree::Value(Value::Number(2.0));
        let result = combine(left.clone(), op, right.clone());
        assert_eq!(result, Tree::Tree {op: Operator::Mul, operands: vec![left, right]});
    }

    #[test]
    fn tree_value_same_operator() {
        let tree_left = Tree::Value(Value::Number(1.0));
        let tree_right = Tree::Value(Value::Number(2.0));
        let tree = Tree::Tree{op: Operator::Mul, operands: vec![tree_left.clone(), tree_right.clone()]};
        let op = Operator::Mul;
        let right = Tree::Value(Value::Number(3.0));
        let result = combine(tree.clone(), op, right.clone());
        assert_eq!(result, Tree::Tree {op: Operator::Mul, operands: vec![tree_left, tree_right, right]});
    }

    #[test]
    fn tree_with_values_different_operator() {
        let tree_left = Tree::Value(Value::Number(1.0));
        let tree_right = Tree::Value(Value::Number(2.0));
        let tree = Tree::Tree{op: Operator::Add, operands: vec![tree_left.clone(), tree_right.clone()]};
        let op = Operator::Mul;
        let right = Tree::Value(Value::Number(3.0));
        let result = combine(tree.clone(), op, right.clone());
        assert_eq!(result, Tree::Tree {op: Operator::Add, operands: vec![tree_left, Tree::Tree{op: Operator::Mul, operands: vec![tree_right, right]}]});
    }

    #[test]
    fn tree_with_inner_tree_same_operator() {
        let inner_tree_left = Tree::Value(Value::Number(1.0));
        let inner_tree_right = Tree::Value(Value::Number(2.0));
        let inner_tree = Tree::Tree{op: Operator::Mul, operands: vec![inner_tree_left.clone(), inner_tree_right.clone()]};
        let outer_tree_left = Tree::Value(Value::Variable("a".to_string()));
        let tree = Tree::Tree {op: Operator::Add, operands: vec![outer_tree_left.clone(), inner_tree.clone()]};
        let next_op = Operator::Mul;
        let next = Tree::Value(Value::Number(3.0));

        let result = combine(tree.clone(), next_op, next.clone());
        assert_eq!(result, Tree::Tree {op: Operator::Add , operands: vec![outer_tree_left, Tree::Tree{op: Operator::Mul, operands: vec![inner_tree_left, inner_tree_right, next]}]});
    }

    #[test]
    fn tree_with_inner_tree_different_operator_matching_outer() {
        let inner_tree_left = Tree::Value(Value::Number(1.0));
        let inner_tree_right = Tree::Value(Value::Number(2.0));
        let inner_tree = Tree::Tree{op: Operator::Mul, operands: vec![inner_tree_left.clone(), inner_tree_right.clone()]};
        let outer_tree_left = Tree::Value(Value::Variable("a".to_string()));
        let tree = Tree::Tree {op: Operator::Add, operands: vec![outer_tree_left.clone(), inner_tree.clone()]};
        let next_op = Operator::Add;
        let next = Tree::Value(Value::Number(3.0));

        let result = combine(tree.clone(), next_op, next.clone());
        assert_eq!(result, Tree::Tree {op: Operator::Add , operands: vec![outer_tree_left, Tree::Tree{op: Operator::Mul, operands: vec![inner_tree_left, inner_tree_right]}, next]});
    }

    #[test]
    fn tree_with_inner_tree_different_dash() {
        let inner_tree_left = Tree::Value(Value::Number(1.0));
        let inner_tree_right = Tree::Value(Value::Number(2.0));
        let inner_tree = Tree::Tree{op: Operator::Mul, operands: vec![inner_tree_left.clone(), inner_tree_right.clone()]};
        let outer_tree_left = Tree::Value(Value::Variable("a".to_string()));
        let tree = Tree::Tree {op: Operator::Add, operands: vec![outer_tree_left.clone(), inner_tree.clone()]};
        let next_op = Operator::Sub;
        let next = Tree::Value(Value::Number(3.0));

        let result = combine(tree.clone(), next_op, next.clone());
        assert_eq!(result, Tree::Tree {op: Operator::Add , operands: vec![outer_tree_left, Tree::Tree{op: Operator::Sub, operands: vec![Tree::Tree{op: Operator::Mul, operands: vec![inner_tree_left, inner_tree_right]}, next]}]});
    }

    #[test]
    fn tree_with_inner_tree_different_point() {
        let inner_tree_left = Tree::Value(Value::Number(1.0));
        let inner_tree_right = Tree::Value(Value::Number(2.0));
        let inner_tree = Tree::Tree{op: Operator::Mul, operands: vec![inner_tree_left.clone(), inner_tree_right.clone()]};
        let outer_tree_left = Tree::Value(Value::Variable("a".to_string()));
        let tree = Tree::Tree {op: Operator::Add, operands: vec![outer_tree_left.clone(), inner_tree.clone()]};
        let next_op = Operator::Div;
        let next = Tree::Value(Value::Number(3.0));

        let result = combine(tree.clone(), next_op, next.clone());
        assert_eq!(result, Tree::Tree {op: Operator::Add , operands: vec![outer_tree_left, Tree::Tree{op: Operator::Mul, operands: vec![inner_tree_left, Tree::Tree{op: Operator::Div, operands: vec![inner_tree_right, next]}]}]});
    }
}