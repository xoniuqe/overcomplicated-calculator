use std::fmt;

#[derive(Debug)]
#[derive(PartialEq, Eq, Clone, PartialOrd)]
pub enum Operator { Add, Sub, Mul, Div }

impl Operator {
    pub fn from_string(input: String) -> Operator {
        match &input[..] {
            "+" => Operator::Add,
            "-" => Operator::Sub,
            "*" => Operator::Mul,
            "/" => Operator::Div,
            _ => panic!("{} is no valid operator string!", input)
        }
    }
}

#[derive(Debug)]
#[derive(PartialEq, Clone)]
pub enum Value { Variable(String), Number(f32) }


#[derive(Debug)]
#[derive(PartialEq, Clone)]
pub enum Token { LeftP, RightP, Operator(Operator), Literal(Value) }

impl fmt::Display for Token {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match self {
            Token::LeftP => write!(f, "LeftP"),
            Token::RightP => write!(f, "RightP"),
            Token::Operator(op) => write!(f, "Operator({:?})", op),
            Token::Literal(value) => write!(f, "Value({:?})", value)
        }
    }
}

pub fn tokenize_string(input: &str) -> Vec<(Token, i32)>
{
    fn append_literal(result: &mut Vec<(Token, i32)>, current: Vec<char>, index: i32) -> Vec<char> {
        if current.len() > 0 {
            let literal : String = current.into_iter().collect();
            let value = match literal.parse() {
                Ok(number) => Value::Number(number),
                _ => Value::Variable(literal)
            };
            result.push((Token::Literal(value), index));
        }
        vec![]
    }

    let mut result = vec![];
    let mut current = vec![];
    let mut index = 0;
    let mut current_index = 0;
    for char in input.chars() {
        match char {
            ' ' => current = append_literal(&mut result, current, current_index),
            '(' => {
                current = append_literal(&mut result, current, current_index);
                result.push((Token::LeftP, index));
            },
            ')' => {
                current = append_literal(&mut result, current, current_index);
                result.push((Token::RightP, index));
            }
            '*' | '+' | '-' | '/' => {
                current = append_literal(&mut result, current, current_index);
                result.push((Token::Operator(Operator::from_string(char.to_string())), index));
            },
            _ => {
                current.push(char);
                current_index = index;
            }
        }
        index += 1;
    }
    append_literal(&mut result, current, current_index);
    result
}


#[cfg(test)]
mod tests {
    use super::{tokenize_string, Value, Token, Operator};

    #[test]
    fn numbers() {
        assert_eq!(tokenize_string("1"), vec![(Token::Literal(Value::Number(1.0)), 0)]);
        assert_eq!(tokenize_string("12"), vec![(Token::Literal(Value::Number(12.0)), 1)]);
    }

    #[test]
    fn ignore_spaces() {
        assert_eq!(tokenize_string("a"), vec![(Token::Literal(Value::Variable("a".to_string())), 0)]);
        assert_eq!(tokenize_string("a  "), vec![(Token::Literal(Value::Variable("a".to_string())), 0)]);
        assert_eq!(tokenize_string(" a "), vec![(Token::Literal(Value::Variable("a".to_string())), 1)]);
        assert_eq!(tokenize_string("  a"), vec![(Token::Literal(Value::Variable("a".to_string())), 2)]);
    }
    #[test]
    fn single_variable() {
        let result = tokenize_string("a");
        assert_eq!(result, vec![(Token::Literal(Value::Variable("a".to_string())),0)]);
    }

    #[test]
    fn simple_term() {
        let result = tokenize_string("a + b");
        assert_eq!(result, vec![(Token::Literal(Value::Variable("a".to_string())),0),(Token::Operator(Operator::Add),2),(Token::Literal(Value::Variable("b".to_string())),4)]);
    }

    #[test]
    fn simple_term_with_parenthesis() {
        let result = tokenize_string("(a*b)");
        assert_eq!(result, vec![(Token::LeftP,0), (Token::Literal(Value::Variable("a".to_string())),1), (Token::Operator(Operator::Mul),2), (Token::Literal(Value::Variable("b".to_string())),3), (Token::RightP,4)]);
    }
}