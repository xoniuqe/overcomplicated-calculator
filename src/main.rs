mod lib;

use lib::lexer::{tokenize_string, Value, Operator};
use lib::parser::{*};


fn main() {
    println!("Math parsing");
    loop {
        let mut line = String::new();
        if line.contains("exit") {
            break;
        }
        let result = std::io::stdin().read_line(&mut line);
        //remove trailing escape
        line.pop();
        match result {
            Err(_) => return,
            Ok(_) => {
                let mut tokens = tokenize_string(&line);
                let x = parse(&mut tokens);
                match x {
                    Ok(t) => {
                        println!("parsed: {}", t);
                        println!("eval: {:?}", eval_tree(t))
                    },
                    Err(msg) =>
                        match msg {
                            msg @ ParseError::Generic(_) => println!("{}", msg),
                            msg @ ParseError::Unexpected(_) => {
                                println!("{}", line);
                                println!("{}", msg)
                            }
                        }
                }
            }
        }
    }
}


fn eval_tree(tree: Tree) -> Result<f32, String> {
    fn apply_operator(op: Operator, left: f32, right: f32) -> f32 {
        match op {
            Operator::Mul => left * right,
            Operator::Div => left / right,
            Operator::Add => left + right,
            Operator::Sub => left - right,
        }
    }
    match tree {
        Tree::Negation(tree) => Ok(-eval_tree(*tree)?),
        Tree::Parenthesis(tree) =>  Ok(eval_tree(*tree)?),
        Tree::Value(value) => match value {
            Value::Number(number) =>  Ok(number),
            _ => Err(format!("no numeric value possible!"))
        }
        Tree::Tree { op, operands } =>{
            let values: Vec<Result<f32, String>> = operands.into_iter().map(|operand| eval_tree(operand)).collect();
            let mut result = *values[0].as_ref()?;
            for i in 1..values.len() {
                result = apply_operator(op.clone(), result, *values[i].as_ref()?);
            }
            Ok(result)
        }
    }
}